create table clients
(
    GUID            varchar(50) charset utf8 not null,
    civilite        varchar(3) charset utf8  null,
    lastname        varchar(20) charset utf8 null,
    firstname       varchar(20) charset utf8 null,
    isCompany       tinyint(1)               null,
    companyName     varchar(50) charset utf8 null,
    companyFonction varchar(50) charset utf8 null,
    adress1         varchar(80) charset utf8 null,
    adress2         varchar(80) charset utf8 null,
    CP              varchar(5)               null,
    town            varchar(50)              null,
    num1            varchar(11)              null,
    num2            varchar(11)              null,
    email           varchar(60) charset utf8 not null,
    dateCreation    date                     null,
    export          tinyint(1)               null,
    constraint GUID
        unique (GUID)
)
    charset = latin1;

alter table clients
    add primary key (GUID);

create table guidClients
(
    GUID      varchar(50) not null,
    nom       varchar(20) null,
    email     varchar(60) null,
    isSociete tinyint(1)  null,
    constraint GUID
        unique (GUID)
)
    charset = utf8mb4;

alter table guidClients
    add primary key (GUID);

create table ville
(
    IdVille       varchar(5)  not null comment 'id = code insee'
        primary key,
    CodePostal    int(5)      not null,
    NomVille      varchar(30) not null,
    IdDepartement varchar(3)  not null comment 'id = code insee'
)
    engine = MyISAM
    charset = latin1;


