<?php
require "./include/header.php";
require"./sql/connectBDD.php";
require("./include/authorized.php");
?>
    <section>
        <form action="./sql/process.php" class="container form" method="POST" id="survey" name="part" onsubmit="return">
            <?php
            require"form/civ.php";
            require"form/firstname.php";
            require"form/lastname.php";
            require"form/adress.php";
            ?>
            <p>Remplissez au moins un numéro de téléphone *</p><br>
            <div class="row">
                <label for="num1">Téléphone Fixe :</label>
                <?php require"form/num1.php";?>
            </div>
            <div class="row">
                <label for="num2">Téléphone Portable :</label>
                <?php require"form/num2.php";?>
            </div>
            <?php require"form/email.php";?>
            <div class="leg"><p>* :champ à saisie obligatoire</p></div>
            <div class="partpro">
                <button name="valider" value="valider" type="submit" class="button"><span>Valider</span></button>
            </div>
        </form>
    </section>
<?php require "./include/footer.php"; ?>
