<?php
session_start();
require "./sql/connectBDD.php";

$_SESSION['client']= TRUE;
if(isset($_GET['q']) && preg_match("%\w{8}-\w{4}-\w{4}-\w{4}-\w{12}%",$_GET["q"])) {
    $GUID = $_GET['q'];
    $_SESSION['GUID'] = $GUID;
    $request = $db->prepare("SELECT count(*) from guidClients where GUID = :GUID");
    $request->execute([":GUID" => $GUID]);
    $guidExist = $request->fetch();
    if ($guidExist[0] > 0) {
        $request = $db->prepare("SELECT count(*) from clients where GUID = :GUID");
        $request->execute([":GUID" => $GUID]);
        $testGuid = $request->fetch();
        if ($testGuid[0] > 0) {
            header('Location:surveyOK.php');
            exit();
        } else {
            $request = $db->prepare("SELECT isSociete from guidClients where GUID = :GUID");
            $request->execute([":GUID" => $GUID]);
            $testSociete = $request->fetch();

            if ($testSociete['isSociete'] == '1') {
                $_SESSION['statut'] = TRUE;
                header('Location:home.php');
                exit();
            } else {
                $_SESSION['statut'] = FALSE;
                header('Location:home.php');
                exit();
            }
        }
    } else {
        header('Location:./error/errorURL.php');
        exit();
    }
}
else{
    header('Location:./error/error404.php');
    exit();
}

