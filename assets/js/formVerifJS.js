//on selectionne le formulaire
const form = document.querySelector("#survey");

//on met un écouteur d'évênments sur le formulaire, la fonction sera lancé quand on validera le formulaire
form.addEventListener('submit', function (e) {
    e.preventDefault();

    // variables
    let test = true;
    const fields = document.querySelectorAll(".champ");
    const select = document.querySelector("select");
    const radios = document.querySelectorAll(".Checkbox");
    let checkboxF = document.getElementById("F");
    let checkboxM = document.getElementById("M");
    //test des checkbox
    for (let radio of radios) {
        if ((checkboxM.checked !== true) && (checkboxF.checked !== true)) {
            test = false;
            radio.classList.add("error")
        } else {
            radio.classList.remove("error")
        }
    }
    //test des champs sauf adresse 2
    for (let field of fields) {
        field.classList.remove("error");
        if (field.type !== "file" && field.value === '') {
            test = false;
            field.classList.add("error")
        }
    }
    //test num particulier car seul un des deux num est obligatoire
    if (document.getElementById("num1")){
        const nums = document.querySelectorAll(".num");
        let num1 = document.getElementById("num1");
        let num2 = document.getElementById("num2");
        for (let num of nums) {
            if ((num1.value !== "file" && num1.value === '') && (num2.value !== "file" && num2.value === '')) {
                test = false;
                num.classList.add("error")
            } else {
                num.classList.remove("error")
            }
        }
    }
    //test ville
    if (select.value === '') {
        test = false;
        select.classList.add("error")
    } else {
        select.classList.remove("error")
    }
    //si test ok on valide sinon on affiche un message d'alerte
    if(test){
        e.target.submit()
    }
    else {
        alert("Merci de remplir tous les champs")
    }
});

