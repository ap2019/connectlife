let towns = document.querySelector("#town");
let CP = document.querySelector('#CP');
CP.addEventListener('input', function (e) {
    towns.innerHTML = '';
    let req = new XMLHttpRequest();
    req.open("GET", "../sql/town.php?CP=" + CP.value, true)
    req.addEventListener("load", function () {
        console.log(req.responseText);
        let communes = JSON.parse(req.responseText);
        for (let commune of communes) {
            let option = document.createElement("option");
            option.innerHTML = commune.NomVille;
            option.value = commune.NomVille;
            towns.appendChild(option)
        }
    })
    req.send(null);
})






