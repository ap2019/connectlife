<?php
require "./include/header.php";
require"./sql/connectBDD.php";
require("./include/authorized.php");
?>
    <section>
        <form action="./sql/process.php" class="container form" method="POST" id="survey" name="pro" onsubmit="return">
            <?php
            require"form/civ.php";
            require"form/lastname.php";
            require"form/firstname.php" ?>
            <div class="row">
                <label for="companyName">nom de la société * :</label>
                <input class="champ" id="champ" type="text" name="companyName"/>
            </div>
            <div class="row">
                <label for="companyFonction">Poste occupé * :</label>
                <input class="champ" id="champ" type="text" name="companyFonction"/>
            </div>
            <?php require "form/adress.php";?>
            <div class="row">
                <label for="num1">Téléphone société * :</label>
                <?php require"form/num1.php"; ?>
            </div>
            <div class="row">
                <label for="num2">Téléphone direct * :</label>
                <?php require"form/num2.php"; ?>
            </div>
            <?php require"form/email.php"; ?>
            <div class="leg"><p>* :champ à saisie obligatoire</p></div>
            <div class="partpro">
                <button name="valider" type="submit" value="valider" class="button"><span>Valider</span></button>
            </div>
        </form>
    </section>
<?php require "./include/footer.php"; ?>