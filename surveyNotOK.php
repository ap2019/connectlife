<?php
require("./include/header.php");
require("./include/authorized.php"); ?>
    <section>
        <div class="container">
            <p class="surveyOK_p" >
                Oops ! Votre questionnaire n'a pas été bien rempli,<br><br>
                <?php echo $_SESSION['civ'] . " " . $_SESSION['firstname'] . " " . $_SESSION['lastname']?>,<br>
                Il semblerait que <?php echo " ". $_SESSION['error'] ." "; ?> que vous avez entré ne soit pas correct.<br><br>
                Veuillez renseigner <?php echo " ". $_SESSION['error2'] ." "; ?>.
            </p><br><br>
            <div class="partpro">
                <button class="button" value="back" onclick="history.go(-1)">
                <span>Retour</span></button>
            </div>
            <p class="surveyOK_p" >Si vous avez des questions, n’hésitez pas à nous contacter : <br><br>
                <?php require "./error/mailto.php"; ?> <br><br>
            </p>
    </section>
<?php require("./include/footer.php"); ?>