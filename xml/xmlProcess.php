<?php
$xmlFile = new DOMDocument('1.0', 'utf-8');
$xmlFile->appendChild($NewClients = $xmlFile->createElement('NewClients'));
$NewClients->setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
$NewClients->setAttribute("xsi:noNamespaceSchemaLocation","NewClients.xsd");
try {
    while($rs = $r->fetch(PDO::FETCH_ASSOC)){
        $NewClients->appendChild($clients = $xmlFile->createElement('clients'));
        $clients->appendChild($xmlFile->createElement('GUID', $rs['GUID']));
        $clients->appendChild($xmlFile->createElement('civilite', $rs['civilite']));
        $clients->appendChild($xmlFile->createElement('lastname', $rs['lastname']));
        $clients->appendChild($xmlFile->createElement('firstname', $rs['firstname']));
        $clients->appendChild($xmlFile->createElement('isCompany', $rs['isCompany']));
        $clients->appendChild($xmlFile->createElement('companyName', $rs['companyName']));
        $clients->appendChild($xmlFile->createElement('companyFonction', $rs['companyFonction']));
        $clients->appendChild($xmlFile->createElement('adress1', $rs['adress1']));
        $clients->appendChild($xmlFile->createElement('adress2', $rs['adress2']));
        $clients->appendChild($xmlFile->createElement('CP', $rs['CP']));
        $clients->appendChild($xmlFile->createElement('town', $rs['town']));
        $clients->appendChild($xmlFile->createElement('num1', $rs['num1']));
        $clients->appendChild($xmlFile->createElement('num2', $rs['num2']));
        $clients->appendChild($xmlFile->createElement('email', $rs['email']));
        $clients->appendChild($xmlFile->createElement('dateCreation', $rs['dateCreation']));
        $clients->appendChild($xmlFile->createElement('export', $rs['export']));
    }
}
catch(Exception $e){
    var_dump($rs);
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}
$xmlFile->formatOutput = true;

