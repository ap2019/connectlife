<?php
require"../include/header.php";
require"../sql/connectBDD.php";
?>
<section xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <p class="surveyOK_p" >
            Choisissez deux dates entre lesquelles les clients ont été ajouté dans la nouvelles bases.<br><br>
        </p>
        <form method="post" class="container form">
            <input class="" type="date" name="date1" value="<?php if (isset($_POST['go'])) {
                echo $_POST['date1'];
            } ?>">
            <input class="" type="date" name="date2" value="<?php if (isset($_POST['go'])) {
                echo $_POST['date2'];
            } ?>">
            <button type="submit" name="go" class="button"><span>VALIDER</span></button>
            <?php
            if (isset($_POST['go'])) {
            $params = [
                ':date1' => $_POST['date1'],
                ':date2' => $_POST['date2']
            ];
            }
            $r = $db->prepare('SELECT * FROM clients WHERE dateCreation BETWEEN :date1 AND :date2');
            $r->execute($params);
            require"xmlProcess.php";
            $xmlFile->save('DateCreationClients.xml');
            ?>
        </form>
        <p class="surveyOK_p" >Pour le télécharger veuillez cliquer sur le bouton ci dessous.<br>
        </p><br><br>
        <div class="partpro">
            <a href="DateCreationClients.xml" download="DateCreationClients.xml" ><button class="button">
                    <span>Télécharger</span></button></a>
        </div>
    </div>
</section>
