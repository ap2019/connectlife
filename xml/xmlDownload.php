<?php
require"../include/header.php";
require"../sql/connectBDD.php";
//selection de tous les clients jamais exporter en xml
$r = $db->query('SELECT * FROM clients WHERE export is NULL');
require "xmlProcess.php";
$xmlFile->save('NewClients.xml');
//on passe ses clients en déjà exporté
$export = $db->query('UPDATE clients SET export = 1 WHERE export is null');
?>
<section>t
    <div class="container">
        <p class="surveyOK_p" >
            Votre questionnaire fichier xml a bien été généré,<br><br>
            Pour le télécharger veuillez cliquer sur le bouton ci dessous.<br>
        </p><br><br>
        <div class="partpro">
            <a href="NewClients.xml" download="NewClients.xml"><button class="button">
                    <span>Télécharger</span></button></a>
        </div>
</section>