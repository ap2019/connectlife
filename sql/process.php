<?php
session_start();
require "connectBDD.php";
//si il y a quelque chose dans le post on lance le process
if (!empty($_POST)) {
    //on détermine les variables de sessions, nom prenom et civ pour la page d'erreur questionnaire mal rempli
    $_SESSION['civ'] = $_POST['checkbox'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['firstname'] = $_POST['firstname'];
    //on verifie les différentes données que l'on a déjà sur le client
    require "../verifData/verifNum.php";
    require "../verifData/verifEmail.php";
    require "../verifData/verifGUID.php";
    //on vérifie si il est pro ou particulier via la session statut
    if ($_SESSION['statut']) {
        $isCompany = 1;
    } else {
        $isCompany = 0;
    }
    //on récupère la date du jour
    date_default_timezone_set('UTC');
    $date = date("y-m-d");
    //on détermine les paramètres de la requête avec ce que l'on a récupéré dans le formulaire
    $params = [
        'GUID' => $_SESSION["GUID"],
        'civilite' => $_POST["checkbox"],
        'lastname' => $_POST["lastname"],
        'firstname' => $_POST["firstname"],
        'isCompany' => $isCompany,
        'companyName' => $_POST["companyName"],
        'companyFonction' => $_POST["companyFonction"],
        'adress1' => $_POST["adress1"],
        'adress2' => $_POST["adress2"],
        'CP' => $_POST["CP"],
        'town' => $_POST["town"],
        'num1' => $_POST["num1"],
        'num2' => $_POST["num2"],
        'email' => $_POST["email"],
        'dateCreation' => $date
    ];
    $req = $db->prepare("INSERT INTO clients (GUID, civilite, lastname, firstname, isCompany, companyName, companyFonction, adress1, adress2, CP, town, num1, num2, email, dateCreation) VALUES (:GUID, :civilite, :lastname, :firstname, :isCompany, :companyName, :companyFonction, :adress1, :adress2, :CP, :town, :num1, :num2, :email, :dateCreation)");
    try {
        $req->execute($params);
    } catch (PDOException $error) {
        print "Erreur !: PARAMS" . $error->getMessage() . "<br/>";
        die();
    }
    $_SESSION['code'] = "Vode codre PROMOTIONNEL ICI : 28687FGH";
    header("Location:../surveyOK.php");
    exit();
}
else {
    header("Location:../error/errorURL.php");
    exit();
}




