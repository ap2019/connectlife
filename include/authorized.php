<?php
try
{
    if (!isset($_SESSION['client']))
        throw new Exception("Vous n'avez pas accès à cette page, veuillez passer par le lien fourni dans l'email");
}
catch (Exception $e) 
{
    echo "<script>alert(\"" . $e->getMessage() . "\")</script>";
    die();
}
