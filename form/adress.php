<div class="row">
    <label for="adress1">Adresse 1 * :</label>
    <input class="champ" id="champ" type="text" name="adress1"/>
</div>
<div class="row">
    <label for="adress2">Adresse 2 :</label>
    <input class="champSpe" type="text" name="adress2"/>
</div>
<div class="row">
    <label for="CP">Code postal * :</label>
    <input class="champ" id="CP" name="CP" type="text" minlength="5" maxlength="5" pattern="[0-9]{4,5}"/>
</div>
<div class="row">
    <label for="town">Ville * :</label>
    <select class="champ" id="town" name="town">
    </select>
</div>
