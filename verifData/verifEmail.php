<?php
//on verifie que l'email existe bien dans la table d'origine et a quel GUID il correspond
$request = $db->prepare("SELECT email from guidClients where GUID = :GUID");
$request->execute([":GUID" => $_SESSION['GUID']]);
$emailExist = $request->fetch();
if ($emailExist['email'] == $_POST['email']) {
} else {
    //si il n'existe pas on lui indique dans la page d'erreur qu'il doit essayer avec le mail avec lequel il a reçu le lien
    $_SESSION['error'] = "adresse email";
    $_SESSION['error2'] = "l'adresse email avec laquelle vous avez reçu le lien";
    header("Location:../surveyNotOK.php");
    exit();
}