<?php
//on test si le guid exite dans la base d'orgine
$request = $db->prepare("SELECT count(*) from guidClients where GUID = :GUID");
$request->execute([":GUID" => $_SESSION['GUID']]);
$guidExist = $request->fetch();
//si le guid existe don va voir si il n'est pas déjà dans la nouvelle base client si c'est le cas on l'envoi vers le questionnaire déjà rempli
if($guidExist[0] > 0){
    $request = $db->prepare("SELECT count(*) from clients where GUID = :GUID");
    $request->execute([":GUID" => $_SESSION['GUID']]);
    $testGuid = $request->fetch();
    if ($testGuid[0] > 0) {
        header('Location:../surveyOK.php');
        exit();
    }
}
//si le guid n'existe pas mais qu'il est arrivé jusque ici il a peut être un problème d'URL donc il pourra nosu contacter via cette page
else{
    header("Location:../errorURL.php");
    exit();
}